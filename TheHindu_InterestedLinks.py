#Importing the libraries
import datetime
import requests
from bs4 import BeautifulSoup
import os

#Output File Location
destinationLocation = "C:\\Users\\bipin.ashok.wanchoo\\Desktop\\Python\\RandomProjects\\TodaysNews.html"

#Determine destinationLocation on the basis of home laptop or office machine
HomeComputerName = "LAPTOP-B18L664O"
if(os.environ['COMPUTERNAME'] == HomeComputerName):
    destinationLocation = "C:\\Users\\Bipin\Desktop\\MyDailyNews.html"


#Keywords for which we want the news
my_keywords = ['animal', 'bird', 'wild', 'fish', 'species', 'sanctuar', 'zoo', 'reserve', 'wing', 'tiger', 'fish', 'endanger', 'IUCN', 'extinct']

#Taking out today's The Hindu's URL
now = datetime.datetime.now()
todays_date = str(now.year)+'/'+str(now.month)+'/'+str(now.day)+'/'
url = 'https://www.thehindu.com/archive/print/'+todays_date

#Assigning background color values
bgcolorHead ="#88CCAA"
bgcolor1="#EEAA11"
bgcolor2="#DDAAFF"

#Creating the HTML File. Body will be updated as per the logic 
HtmlFileObj = open(destinationLocation,'w')
message_prefix = "<html><head></head><body><table border=1><tr bgcolor='"+bgcolorHead+"'><th>Key</th><th>News Link</th><th>News Content</th></tr>"
message_body = ""
message_suffix = "</table></body></html>"
count=0
#Logic
response = requests.get(url)

if response.status_code==200:
    soup = BeautifulSoup(response.text, 'html.parser')
    allnewslinks=soup.find_all("ul",{"class":"archive-list"})
    
    for c in allnewslinks:
        for i in c.findAll("a"):
            for key in my_keywords:
                url2 = i.attrs['href']
                url2id = url2[len(url2) - 12:len(url2) - 4]
                linkbody=""
                if key in i.attrs['href'] or key in i.text :
                    #News Link content Logic
                    response2 = requests.get(url2)
                    if response2.status_code==200:
                        bgcolor = bgcolor1
                        if (count %2 ==1):
                            bgcolor = bgcolor2
                        soup = BeautifulSoup(response2.text, 'html.parser')
                        linkbody=str(soup.find_all("div",{"id":"content-body-14269002-"+url2id}))
                        linkbody = linkbody[1 : len(linkbody)-1] #To remove the '[' and ']' in the news content body
                        count = count+1
                    message_body = message_body+"<tr bgcolor="+bgcolor+"'><td>"+key+"</td><td><a href='"+i.attrs['href']+"'>"+i.text+"</a>"+"</td><td>"+str(linkbody)+"</td></tr>"
        
else:
    print("Error")

#Writing all to HTML file
HtmlFileObj.write(message_prefix+message_body+message_suffix)
HtmlFileObj.close()
